/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// editor support
import 'v-markdown-editor/dist/index.css';
import Editor from 'v-markdown-editor'
Vue.use(Editor);

import VueMoment from 'vue-moment';

Vue.use(VueMoment)

// Support vuex
import Vuex from 'vuex'
Vue.use(Vuex)
import storeData from "./store/index"
const store = new Vuex.Store(
    storeData
)

//support moment js
import {filter} from './filter'

import VueRouter from 'vue-router';

Vue.use(VueRouter); 

import {routes} from './routes';

import EventBus from './EventBus';

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// V-form
import { Form, HasError, AlertError } from 'vform'
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
window.Form = Form;


// Sweet alert 2
import Swal from 'sweetalert2'
window.swal = Swal;
const Toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.toast = Toast


const router = new VueRouter({
    routes,
    mode: 'hash',

});

const app = new Vue({
    el: '#app',
    router,
    store,
    EventBus,
});
