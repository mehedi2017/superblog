<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Post extends Eloquent
{
    
    public  function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public  function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
}
