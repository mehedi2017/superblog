<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Category extends Eloquent
{
    
    public function posts(){
        return $this->hasMany(Post::class,'id');
    }
}
