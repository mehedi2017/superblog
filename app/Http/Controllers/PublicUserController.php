<?php
namespace App\Http\Controllers;
use App\PublicUser;
use Illuminate\Http\Request;
class PublicUserController extends Controller
{
    
    public function signup(Request $request) {
		
        $this->validate($request,[
            'full_name'=>'required|min:2|max:50',
            'email' => 'required|string|email|max:255|unique:public_users',
            'password'=>'required|confirmed',
        ]);
       
       $publicUser = New PublicUser();
       $publicUser->name = $request->full_name;
       $publicUser->email = $request->email;
       $publicUser->password = $request->password;

       $publicUser->save();
       return response()->json([
        'status'=>'success',
        'data' => []
        ],200);
    }

    public function signin(Request $request) {
        
        $this->validate($request,[
            'email' => 'required|string|email|max:255',
            'password'=>'required',
        ]);

        $login = PublicUser::where(['email'=>$request->email,'password'=>$request->password])->first();

        if(!is_null($login)) {
            return response()->json([
                'status'=>'success',
                'userid' => $login->_id,
                'username'=> $login->name
            ],200);
        }
        else {
            return response()->json([
                'status'=>'error',
                'userid' => null
            ],200);
        }
    }
    
}